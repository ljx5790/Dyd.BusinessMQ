﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BSF.BaseService.BusinessMQ.SystemRuntime;

namespace BusinessMQ.Web.Base
{
    public class RedisHelper
    {
        public static void SendMessage(string redisserverip, string channal,BusinessMQNetCommand command)
        {
            var manager = new BSF.Redis.RedisManager();
            using (var c = manager.GetPoolClient(redisserverip, 1, 1))
            {
                var i = c.GetClient().PublishMessage(channal, new BSF.Serialization.JsonProvider().Serializer(command));
            }
        }
    }
}